import math

def solve_quadratic_equation(a, b, c):
    # Calculate the discriminant
    discriminant = b**2 - 4*a*c

    # Check if the discriminant is non-negative for real roots
    if discriminant >= 0:
        # Calculate the two roots
        root1 = (-b + math.sqrt(discriminant)) / (2*a)
        root2 = (-b - math.sqrt(discriminant)) / (2*a)

        return root1, root2
    else:
        # If discriminant is negative, roots are complex
        real_part = -b / (2*a)
        imaginary_part = math.sqrt(abs(discriminant)) / (2*a)

        root1 = complex(real_part, imaginary_part)
        root2 = complex(real_part, -imaginary_part)

        return root1, root2

# Example usage:
a = float(input("Enter coefficient a: "))
b = float(input("Enter coefficient b: "))
c = float(input("Enter coefficient c: "))

roots = solve_quadratic_equation(a, b, c)

print("Roots:", roots)
